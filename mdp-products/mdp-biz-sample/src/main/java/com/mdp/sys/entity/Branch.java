package  com.mdp.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mdp.core.dao.annotation.TableIds;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 组织 com  顶级模块 mdp 大模块 sys  小模块 <br> 
 * 实体 Branch所有属性名: <br>
 *	"id","机构编号","branchName","机构名称","enabled","是否可用","industryCategory","行业分类","cuserid","创建人编号-可以转让,创建人与机构管理员有同样的权限","cdate","创建日期","cusername","创建人姓名-可以转让","lphoneNo","联系电话","emaill","邮件","bizProcInstId","当前流程实例编号","bizFlowState","当前流程状态","pbranchId","上级机构","admUserid","管理员编号（==机构编号，不允许修改，即机构主账户）","admUsername","管理员名称（==机构名称+'管理员'，不允许修改）","lusername","联系人姓名","luserid","联系人编号","address","公司地址","btype","机构类别0-个人虚拟机构，1-实体机构，个人虚拟机构的话sys_branch表没有真正的机构数据","imgUrl","企业头像","bcode","税号-统一信用识别号","blicense","营业执照图片","legalPerson","法人名称","regCapital","注册资本","remark","企业简介","validLvls","人工验证结果，当审核状态为2时，同步到sys_user表同一个字段，或者sys_branch同一个字段";<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 */
 @Data
@TableName("sys_branch")
@ApiModel(description="管理端机构表（机构下面若干部门）")
public class Branch  implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	@TableIds
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(notes="机构编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;
	
	@ApiModelProperty(notes="机构名称",allowEmptyValue=true,example="",allowableValues="")
	String branchName;
	
	@ApiModelProperty(notes="是否可用",allowEmptyValue=true,example="",allowableValues="")
	String enabled;
	
	@ApiModelProperty(notes="行业分类",allowEmptyValue=true,example="",allowableValues="")
	String industryCategory;

	@TableIds
	@ApiModelProperty(notes="创建人编号-可以转让,创建人与机构管理员有同样的权限",allowEmptyValue=true,example="",allowableValues="")
	String cuserid;
	
	@ApiModelProperty(notes="创建日期",allowEmptyValue=true,example="",allowableValues="")
	Date cdate;

	@TableField("cusername_")
	@ApiModelProperty(notes="创建人姓名-可以转让",allowEmptyValue=true,example="",allowableValues="")
	String cusername;
	@TableField("res222.lphone_no")
	@ApiModelProperty(notes="联系电话",allowEmptyValue=true,example="",allowableValues="")
	String lphoneNo;
	
	@ApiModelProperty(notes="邮件",allowEmptyValue=true,example="",allowableValues="")
	String emaill;
	
	@ApiModelProperty(notes="当前流程实例编号",allowEmptyValue=true,example="",allowableValues="")
	String bizProcInstId;
	
	@ApiModelProperty(notes="当前流程状态",allowEmptyValue=true,example="",allowableValues="")
	String bizFlowState;
	
	@ApiModelProperty(notes="上级机构",allowEmptyValue=true,example="",allowableValues="")
	String pbranchId;
	
	@ApiModelProperty(notes="管理员编号（==机构编号，不允许修改，即机构主账户）",allowEmptyValue=true,example="",allowableValues="")
	String admUserid;
	
	@ApiModelProperty(notes="管理员名称（==机构名称+'管理员'，不允许修改）",allowEmptyValue=true,example="",allowableValues="")
	String admUsername;
	
	@ApiModelProperty(notes="联系人姓名",allowEmptyValue=true,example="",allowableValues="")
	String lusername;
	
	@ApiModelProperty(notes="联系人编号",allowEmptyValue=true,example="",allowableValues="")
	String luserid;
	
	@ApiModelProperty(notes="公司地址",allowEmptyValue=true,example="",allowableValues="")
	String address;
	
	@ApiModelProperty(notes="机构类别0-个人虚拟机构，1-实体机构，个人虚拟机构的话sys_branch表没有真正的机构数据",allowEmptyValue=true,example="",allowableValues="")
	String btype;
	
	@ApiModelProperty(notes="企业头像",allowEmptyValue=true,example="",allowableValues="")
	String imgUrl;
	
	@ApiModelProperty(notes="税号-统一信用识别号",allowEmptyValue=true,example="",allowableValues="")
	String bcode;
	
	@ApiModelProperty(notes="营业执照图片",allowEmptyValue=true,example="",allowableValues="")
	String blicense;
	
	@ApiModelProperty(notes="法人名称",allowEmptyValue=true,example="",allowableValues="")
	String legalPerson;
	
	@ApiModelProperty(notes="注册资本",allowEmptyValue=true,example="",allowableValues="")
	BigDecimal regCapital;
	
	@ApiModelProperty(notes="企业简介",allowEmptyValue=true,example="",allowableValues="")
	String remark;
	
	@ApiModelProperty(notes="人工验证结果，当审核状态为2时，同步到sys_user表同一个字段，或者sys_branch同一个字段",allowEmptyValue=true,example="",allowableValues="")
	String validLvls;

	/**
	 *机构编号
	 **/
	public Branch(String id) {
		this.id = id;
	}
    
    /**
     * 管理端机构表（机构下面若干部门）
     **/
	public Branch() {
	}

}