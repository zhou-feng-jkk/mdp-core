package com.mdp.safe.client.entity;

import com.mdp.core.dao.annotation.TableIds;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 组织 com  顶级模块 mdp 大模块 menu  小模块 <br> 
 * 实体 MenuModuleBranch所有属性名: <br>
 *	branchId,moduleName,moduleId,startTime,endTime,ctime,ltime,cuserid,cusername,luserid,lusername,status,musers,mbidNum,sfeeRate,ver,feeDate,crowd;<br>
 * 表 menu_module_branch 机构开通模块对应关系表的所有字段名: <br>
 *	branch_id,module_name,module_id,start_time,end_time,ctime,ltime,cuserid,cusername,luserid,lusername,status,musers,mbid_num,sfee_rate,ver,fee_date,crowd;<br>
 * 当前主键(包括多主键):<br>
 *	branch_id,module_id;<br>
 */
@Data
@ApiModel(description="机构开通模块对应关系表")
public class ModuleBranch implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	@TableIds
	@ApiModelProperty(notes="机构编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String branchId;
	@TableIds
	@ApiModelProperty(notes="模块编号,主键",allowEmptyValue=true,example="",allowableValues="")
	String moduleId;


	@ApiModelProperty(notes="模块名称",allowEmptyValue=true,example="",allowableValues="")
	String moduleName;

	@ApiModelProperty(notes="启用日期",allowEmptyValue=true,example="",allowableValues="")
	Date startTime;

	@ApiModelProperty(notes="结束日期",allowEmptyValue=true,example="",allowableValues="")
	Date endTime;

	@ApiModelProperty(notes="创建日期",allowEmptyValue=true,example="",allowableValues="")
	Date ctime;

	@ApiModelProperty(notes="更新日期",allowEmptyValue=true,example="",allowableValues="")
	Date ltime;

	@ApiModelProperty(notes="创建人编号",allowEmptyValue=true,example="",allowableValues="")
	String cuserid;

	@ApiModelProperty(notes="创建人姓名",allowEmptyValue=true,example="",allowableValues="")
	String cusername;

	@ApiModelProperty(notes="修改人编号",allowEmptyValue=true,example="",allowableValues="")
	String luserid;

	@ApiModelProperty(notes="修改人姓名",allowEmptyValue=true,example="",allowableValues="")
	String lusername;

	@ApiModelProperty(notes="状态0停用1启用",allowEmptyValue=true,example="",allowableValues="")
	String status;

	@ApiModelProperty(notes="购买用户数",allowEmptyValue=true,example="",allowableValues="")
	Integer musers;

	@ApiModelProperty(notes="可投标次数-每月恢复为套餐数量，包含公司账户次数+个人以组织名义接单的次数，以上每接一单扣减此处",allowEmptyValue=true,example="",allowableValues="")
	Integer mbidNum;

	@ApiModelProperty(notes="服务费率，15=15%",allowEmptyValue=true,example="",allowableValues="")
	Integer sfeeRate;

	@ApiModelProperty(notes="企业购买的版本0-免费版，1-企业版",allowEmptyValue=true,example="",allowableValues="")
	String ver;

	@ApiModelProperty(notes="开始计费日期",allowEmptyValue=true,example="",allowableValues="")
	Date feeDate;

	@ApiModelProperty(notes="是否为众包",allowEmptyValue=true,example="",allowableValues="")
	String crowd;

}