package com.mdp.dev.entity;

import org.springframework.jdbc.support.JdbcUtils;


public class FieldInfo {
	
	private String tableRemarks="";
	public String fullJavaClassName;//字段的所属类的全名,包括包名 如java.lang.String
	private String chinaName;//字段的所属类的中文名字,来自字段中文名或者字段注释.
	public String simpleJavaClassName;//字段的所属类的名名,不包括包名 如String
	public String bigColumnName;//首字母大写
	public String camelsColumnName;//驼峰命名法
	public String bigCamelsColumnName;//首字母大写的驼峰命名法

	public String getBigCamelsColumnName() {
		return bigCamelsColumnName;
	}

	public void setBigCamelsColumnName(String bigCamelsColumnName) {
		this.bigCamelsColumnName = bigCamelsColumnName;
	}


	public String tableSchemaName ; //表模式（可能为空）,在oracle中获取的是命名空间,其它数据库未知     
	public String tableName;  //表名  
	public String columnName;  //列名  
	public int dataType ;     //对应的java.sql.Types的SQL类型(列类型ID)     
	public String dataTypeName ;  //java.sql.Types类型名称(列类型名称)
	public int columnSize ;  //列大小  
	public int decimalDigits;  //小数位数 
	public int numPrecRadix ;  //基数（通常是10或2） --未知
    /**
     *  0 (columnNoNulls) - 该列不允许为空
     *  1 (columnNullable) - 该列允许为空
     *  2 (columnNullableUnknown) - 不确定该列是否为空
     */
	public int nullAble ;  //是否允许为null  
	private String remarks="";  //列描述  
	public String columnDef;  //默认值  
	public int charOctetLength ;    // 对于 char 类型，该长度是列中的最大字节数 
	public int ordinalPosition ;   //表中列的索引（从1开始）  
    /** 
     * ISO规则用来确定某一列的是否可为空(等同于NULLABLE的值:[ 0:'YES'; 1:'NO'; 2:''; ])
     * YES -- 该列可以有空值; 
     * NO -- 该列不能为空;
     * 空字符串--- 不知道该列是否可为空
     */  
	public String isNullAble ;  
      
    /** 
     * 指示此列是否是自动递增 
     * YES -- 该列是自动递增的
     * NO -- 该列不是自动递增
     * 空字串--- 不能确定该列是否自动递增
     */  
    //String isAutoincrement = rs.getString("IS_AUTOINCREMENT");   //该参数测试报错    
    
	public Object testValue;
	public boolean isPk=false;
	
	

	public boolean isPk() {
		return isPk;
	}

	public void setPk(boolean isPk) {
		this.isPk = isPk;
	}

	public FieldInfo() {}

    public String getTableSchemaName() {
		return tableSchemaName;
	}
	public void setTableSchemaName(String tableSchemaName) {
		this.tableSchemaName = tableSchemaName;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
		this.bigColumnName=columnName.substring(0, 1).toUpperCase()+this.columnName.substring(1);
		this.camelsColumnName=JdbcUtils.convertUnderscoreNameToPropertyName(columnName);
		this.bigCamelsColumnName=camelsColumnName.substring(0, 1).toUpperCase()+camelsColumnName.substring(1);
	}
	public int getDataType() {
		return dataType;
	}
	public void setDataType(int dataType) {
		this.dataType = dataType;
	}
	public String getDataTypeName() {
		return dataTypeName;
	}
	public void setDataTypeName(String dataTypeName) {
		this.dataTypeName = dataTypeName;
	}
	public int getColumnSize() {
		return columnSize;
	}
	public void setColumnSize(int columnSize) {
		if(columnSize>=1000){
			this.columnSize=1000;
		}
		this.columnSize = columnSize;
	}
	public int getDecimalDigits() {
		return decimalDigits;
	}
	public void setDecimalDigits(int decimalDigits) {
		this.decimalDigits = decimalDigits;
	}
	public int getNumPrecRadix() {
		return numPrecRadix;
	}
	public void setNumPrecRadix(int numPrecRadix) {
		this.numPrecRadix = numPrecRadix;
	}
	public int getNullAble() {
		return nullAble;
	}
	public void setNullAble(int nullAble) {
		this.nullAble = nullAble;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		if(remarks==null){
			this.remarks="";
		}else{
			this.remarks = remarks.replaceAll("[ \\t\\n\\x0B\\f\\r]\\S*", "");
		}
		
	}
	
	public String getCamelsColumnName() {
		return camelsColumnName;
	}

	public void setCamelsColumnName(String camelsColumnName) {
		this.camelsColumnName = camelsColumnName;
	}

	public String getColumnDef() {
		return columnDef;
	}
	public void setColumnDef(String columnDef) {
		this.columnDef = columnDef;
	}
	public int getCharOctetLength() {
		return charOctetLength;
	}
	public void setCharOctetLength(int charOctetLength) {
		this.charOctetLength = charOctetLength;
	}
	public int getOrdinalPosition() {
		return ordinalPosition;
	}
	public void setOrdinalPosition(int ordinalPosition) {
		this.ordinalPosition = ordinalPosition;
	}
	public String getIsNullAble() {
		return isNullAble;
	}
	public void setIsNullAble(String isNullAble) {
		this.isNullAble = isNullAble;
	}
	public String getFullJavaClassName() {
		return fullJavaClassName;
	}

	public void setFullJavaClassName(String fullJavaClassName) {
		this.fullJavaClassName = fullJavaClassName;
		this.simpleJavaClassName=fullJavaClassName.contains(".")?fullJavaClassName.substring(fullJavaClassName.lastIndexOf(".")+1):fullJavaClassName;
	}

	public String getSimpleJavaClassName() {
		return simpleJavaClassName;
	}

	public void setSimpleJavaClassName(String simpleJavaClassName) {
		this.simpleJavaClassName = simpleJavaClassName;
	}

	public String getBigColumnName() {
		return bigColumnName;
	}

	public void setBigColumnName(String bigColumnName) {
		this.bigColumnName = bigColumnName;
	}

	public  Object getTestValue() {
		
		return this.testValue;
	}

	public String getChinaName() {
		return chinaName;
	}

	public void setChinaName(String chinaName) {
		if(chinaName==null){
			this.chinaName="";
		}else{
			this.chinaName = chinaName.replaceAll("[ \\t\\n\\x0B\\f\\r]\\S*", "");
		}
	}
 

	public static void main(String[] args) {
		FieldInfo fi=new FieldInfo();
		fi.setColumnName("_T__CC_DD_");
		System.out.println(fi.camelsColumnName + "------"+fi.bigCamelsColumnName);
	}

	public String getTableRemarks() {
		return tableRemarks;
	}

	public void setTableRemarks(String tableRemarks) {
		if(tableRemarks==null){
			this.tableRemarks="";
		}else{
			this.tableRemarks = tableRemarks.replaceAll("[ \\t\\n\\x0B\\f\\r]\\S*", "");
		}
	}
	
}
