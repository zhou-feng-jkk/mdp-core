package com.mdp.dev.dao;

import com.mdp.dev.utils.SafePasswordEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.util.DigestUtils;

import java.sql.*;


/**
 * 
 * @author cyc 将entity的名字改成符合驼峰命名规范的名字 20150215
 * @see org.springframework.jdbc.support.JdbcUtils
 *
 */
@Repository
public class UserDao implements InitializingBean{
	
	SafePasswordEncoder passwordEncoder=new SafePasswordEncoder();
	  //获得驱动  
	@Value("${spring.datasource.driver-class-name}")
    public  String driverClassName = "";  
    
	//获得url  
	@Value("${spring.datasource.jdbc-url}")
    public  String url = "";  
    
	//获得连接数据库的用户名  
	@Value("${spring.datasource.username}")
    public  String username = "";
	
	//获得连接数据库的密码  
	@Value("${spring.datasource.password}")
    public  String password = "";  
	
	public Logger logger = LoggerFactory.getLogger(UserDao.class);
	
	public Connection getConnection() {
		Connection conn = null;
		try {
			
			conn = DriverManager.getConnection(url,username,password);
			 /**
		      * 设置连接属性,使得可获取到列的REMARK(备注)
		      
			   if(conn instanceof OracleConnection){
				   ((OracleConnection)conn).setRemarksReporting(true); 
			   }*/
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			 
			e.printStackTrace();
		}  
        
		return conn;
	}

	public void closeConn(Connection conn) {
		if (conn != null)
			try {
				if (!conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
	}

	public void closeRS(Connection conn, ResultSet rs) {
		if (conn != null)
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
	}

 

	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			// 初始化JDBC驱动并让驱动加载到jvm中
			Class.forName(driverClassName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public String getNewMd5Password(String password){
		return passwordEncoder.encode(password);
	}
   
   /**Authorized filter invocation [POST /oauth2/token?grant_type=password&auth_type=password_display_userid&userloginid=superAdmin&password=21218cca77804d2ba1922c33e0151105] with attributes [authenticated]
    *  添加超级管理员
    *  superAdmin
    *  21218cca77804d2ba1922c33e0151105
	*  21218cca77804d2ba1922c33e0151105
    */
   public String initSuperAdminPassword(){ 
	   String displayUserid="superAdmin";
	   String passwordJsMd5=DigestUtils.md5DigestAsHex(("888888").getBytes());
	   System.out.printf("前端MD5后的密码：", passwordJsMd5);//0c85ff2f8718ab9b151e105248b893b9
	   String passwordJavaMd5=getNewMd5Password(passwordJsMd5);
	   System.out.printf("后端加密后密码 ：", passwordJavaMd5);//$2a$10$IRNwiubbXKIahfwhZr3UuuBE0bkncMTmmYgmo.eyiFvpBzq15qs7a
	     String sql="update adm.sys_user u set u.password='"+passwordJavaMd5+"' where u.display_userid='"+displayUserid+"'";
	   insert(sql);
	   return sql;
   }
   
   public void insert(String sql){
	   Connection conn = getConnection();
	Statement st = null;
	
	try{
		 
		//得到运行环境
		st = conn.createStatement();

		//执行SQL
		 st.executeUpdate(sql);
 
	}catch(Exception ex){
		 logger.error("",ex);
	}finally{
		//释放资源
		if(st != null){
			try {
				st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}finally{
				st = null;//--> 让他迅速成为java gc的对象
			}
		}
		if(conn != null){
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}finally{
				conn = null;//--> 让他迅速成为java gc的对象
			}
		}
	}
   }
}