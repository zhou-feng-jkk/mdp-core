package com.mdp.mq.sp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class ImPublish { 
	
	@Autowired
	RedisTemplate redisTemplate;
	
	void  push(String subject,Object  message){ 
		redisTemplate.convertAndSend(subject, message);
	}
	 
}
