package com.mdp.mq.sp;

import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.Topic;
import org.springframework.stereotype.Service;

@Service
public class ImChannelConfig implements ChannelConfig {

	@Override
	public RedisMessageListenerContainer container(RedisMessageListenerContainer container) { 
		
		container.addMessageListener(messageListener(), topic("im")); 
		return container;
	}

	
	MessageListener messageListener() {
		ImSubscriber sub=new ImSubscriber();
		ImMessageListener listener=new ImMessageListener();
		listener.addSubscriber(sub);
		return listener;
	}
	
	Topic topic(String channelName){
		ChannelTopic topic=new ChannelTopic(channelName);
		return topic;
		
	}


	@Override
	public void setMessageListener(MessageListener messageListener, Topic topic) {
		
		
	}


	@Override
	public void removeMessageListener(MessageListener messageListener, Topic topic) {
		
		
	}


	@Override
	public void removeMessageListener(MessageListener messageListener) {
		
		
	}

	@Override
	public void removeMessageListener(String listenerName, Topic topic) {

	}

	@Override
	public void addMessageListener(String listenerName, Topic topic) {

	}
}
