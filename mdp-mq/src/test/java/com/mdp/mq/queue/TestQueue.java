package com.mdp.mq.queue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestQueue {
	
	@Autowired
	Push push;
	
	@Autowired
	MsgList msg;
	
	Logger log= LoggerFactory.getLogger(TestQueue.class);
	
	@Test
	public  void testPush(){ 
		Date d=new Date();
		 for(int i=0;i<100;i++) {
			 
			 push.leftPush("test01","xxxxxxxxxxxx+"+i+" 秒："+d.getSeconds()+"秒"); 
		 }
		
	}  
	
	@Test
	public  void testMessageList(){ 
		msg.popMessage();
		System.out.println("xxxxxxxxxxxx");
	}  
}
