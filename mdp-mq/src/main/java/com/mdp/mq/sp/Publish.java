package com.mdp.mq.sp;

import com.mdp.mq.sp.def.DefaultChanelConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class Publish {
	
	@Autowired
	RedisTemplate redisTemplate;
	
	public void  push(String subject,Object  message){
		redisTemplate.convertAndSend(subject, message);
	}

	public void push(Object message){
		this.push(DefaultChanelConfig.DEFAULT_TOPIC_NAME,message);
	}
}
