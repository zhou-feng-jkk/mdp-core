package com.mdp.mq.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public abstract class MessageListener<T> {
	
	@Autowired
	RedisTemplate redisTemplate;
	
	ExecutorService e = Executors.newFixedThreadPool(3);
	
	public abstract Object getQueueKey();
	
	public abstract void handleMessage(T message);
	
	/*
	 * 毫秒
	 */
	public Long timeOut() {
		return (long) 1000000;
	}
	 
	 
	public void popMessage() { 
		e.execute(new Runnable() {
				public void run() {
				while(true) {
					try {
						T  message=(T) redisTemplate.opsForList().rightPop(getQueueKey(), timeOut(), TimeUnit.MILLISECONDS);  
						handleMessage(message);  
					} catch (Exception e) {
						 
					}
				}

				}
			}); 
	}
}
