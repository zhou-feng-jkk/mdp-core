package com.mdp.mq.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class Push {
	
	@Autowired
	RedisTemplate redisTemplate;
	
	public Long leftPush(Object key,Object value){
		return redisTemplate.opsForList().leftPush(key, value);
	}
}
