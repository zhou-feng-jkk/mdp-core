package com.mdp.audit.log.client.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * 组织 com.qqkj  顶级模块 audit 大模块 base  小模块 <br> 
 * 实体 OperLog所有属性名: <br>
 *	id,userid,username,opTime,firstMenu,secondMenu,func,funcDesc,ip,ctime,params,branchId,branchName,execResult,opType,httpMethod,reqUrl,clientInfo,deptid,deptName;<br>
 * 表 MAUDIT.audit_oper_log audit_oper_log的所有字段名: <br>
 *	id,userid,username,op_time,first_menu,second_menu,func,func_desc,ip,ctime,params,branch_id,branch_name,exec_result,op_type,http_method,req_url,client_info,deptid,dept_name;<br>
 * 当前主键(包括多主键):<br>
 *	id;<br>
 */
@ApiModel(description="audit_oper_log")
public class OperLog implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes="id,主键",allowEmptyValue=true,example="",allowableValues="")
	String id;

	@ApiModelProperty(notes="系统分类编号",allowEmptyValue=true,example="",allowableValues="")
	String categoryId;

	@ApiModelProperty(notes="系统分类名称",allowEmptyValue=true,example="",allowableValues="")
	String categoryName;
	
	@ApiModelProperty(notes="用户id",allowEmptyValue=true,example="",allowableValues="")
	String userid;
	
	@ApiModelProperty(notes="用户名称",allowEmptyValue=true,example="",allowableValues="")
	String username;
	
	@ApiModelProperty(notes="响应时间",allowEmptyValue=true,example="",allowableValues="")
	Integer opTime;
	
	@ApiModelProperty(notes="一级菜单",allowEmptyValue=true,example="",allowableValues="")
	String firstMenu;
	
	@ApiModelProperty(notes="二级菜单",allowEmptyValue=true,example="",allowableValues="")
	String secondMenu;
	
	@ApiModelProperty(notes="功能，菜单按钮",allowEmptyValue=true,example="",allowableValues="")
	String func;
	
	@ApiModelProperty(notes="功能描述",allowEmptyValue=true,example="",allowableValues="")
	String funcDesc;
	
	@ApiModelProperty(notes="IP地址",allowEmptyValue=true,example="",allowableValues="")
	String ip;
	
	@ApiModelProperty(notes="操作时间",allowEmptyValue=true,example="",allowableValues="")
	Date ctime;
	
	@ApiModelProperty(notes="请求参数",allowEmptyValue=true,example="",allowableValues="")
	String params;
	
	@ApiModelProperty(notes="机构编号",allowEmptyValue=true,example="",allowableValues="")
	String branchId;
	
	@ApiModelProperty(notes="机构名称",allowEmptyValue=true,example="",allowableValues="")
	String branchName;
	
	@ApiModelProperty(notes="执行结果",allowEmptyValue=true,example="",allowableValues="")
	String execResult;
	
	@ApiModelProperty(notes="c-r-u-d-down-up",allowEmptyValue=true,example="",allowableValues="")
	String opType;
	
	@ApiModelProperty(notes="http请求方式post/get",allowEmptyValue=true,example="",allowableValues="")
	String httpMethod;
	
	@ApiModelProperty(notes="http请求地址",allowEmptyValue=true,example="",allowableValues="")
	String reqUrl;
	
	@ApiModelProperty(notes="客户端信息",allowEmptyValue=true,example="",allowableValues="")
	String clientInfo;
	
	@ApiModelProperty(notes="归属部门编号",allowEmptyValue=true,example="",allowableValues="")
	String deptid;
	
	@ApiModelProperty(notes="归属部门名称",allowEmptyValue=true,example="",allowableValues="")
	String deptName;

	/**id**/
	public OperLog(String id) {
		this.id = id;
	}
    
    /**audit_oper_log**/
	public OperLog() {
	}
	
	/**
	 * id
	 **/
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 用户id
	 **/
	public void setUserid(String userid) {
		this.userid = userid;
	}
	/**
	 * 用户名称
	 **/
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * 响应时间
	 **/
	public void setOpTime(Integer opTime) {
		this.opTime = opTime;
	}
	/**
	 * 一级菜单
	 **/
	public void setFirstMenu(String firstMenu) {
		this.firstMenu = firstMenu;
	}
	/**
	 * 二级菜单
	 **/
	public void setSecondMenu(String secondMenu) {
		this.secondMenu = secondMenu;
	}
	/**
	 * 功能，菜单按钮
	 **/
	public void setFunc(String func) {
		this.func = func;
	}
	/**
	 * 功能描述
	 **/
	public void setFuncDesc(String funcDesc) {
		this.funcDesc = funcDesc;
	}
	/**
	 * IP地址
	 **/
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * 操作时间
	 **/
	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}
	/**
	 * 请求参数
	 **/
	public void setParams(String params) {
		this.params = params;
	}
	/**
	 * 机构编号
	 **/
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	/**
	 * 机构名称
	 **/
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	/**
	 * 执行结果
	 **/
	public void setExecResult(String execResult) {
		this.execResult = execResult;
	}
	/**
	 * c-r-u-d-down-up
	 **/
	public void setOpType(String opType) {
		this.opType = opType;
	}
	/**
	 * http请求方式post/get
	 **/
	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}
	/**
	 * http请求地址
	 **/
	public void setReqUrl(String reqUrl) {
		this.reqUrl = reqUrl;
	}
	/**
	 * 客户端信息
	 **/
	public void setClientInfo(String clientInfo) {
		this.clientInfo = clientInfo;
	}
	/**
	 * 归属部门编号
	 **/
	public void setDeptid(String deptid) {
		this.deptid = deptid;
	}
	/**
	 * 归属部门名称
	 **/
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	
	/**
	 * id
	 **/
	public String getId() {
		return this.id;
	}
	/**
	 * 用户id
	 **/
	public String getUserid() {
		return this.userid;
	}
	/**
	 * 用户名称
	 **/
	public String getUsername() {
		return this.username;
	}
	/**
	 * 响应时间
	 **/
	public Integer getOpTime() {
		return this.opTime;
	}
	/**
	 * 一级菜单
	 **/
	public String getFirstMenu() {
		return this.firstMenu;
	}
	/**
	 * 二级菜单
	 **/
	public String getSecondMenu() {
		return this.secondMenu;
	}
	/**
	 * 功能，菜单按钮
	 **/
	public String getFunc() {
		return this.func;
	}
	/**
	 * 功能描述
	 **/
	public String getFuncDesc() {
		return this.funcDesc;
	}
	/**
	 * IP地址
	 **/
	public String getIp() {
		return this.ip;
	}
	/**
	 * 操作时间
	 **/
	public Date getCtime() {
		return this.ctime;
	}
	/**
	 * 请求参数
	 **/
	public String getParams() {
		return this.params;
	}
	/**
	 * 机构编号
	 **/
	public String getBranchId() {
		return this.branchId;
	}
	/**
	 * 机构名称
	 **/
	public String getBranchName() {
		return this.branchName;
	}
	/**
	 * 执行结果
	 **/
	public String getExecResult() {
		return this.execResult;
	}
	/**
	 * c-r-u-d-down-up
	 **/
	public String getOpType() {
		return this.opType;
	}
	/**
	 * http请求方式post/get
	 **/
	public String getHttpMethod() {
		return this.httpMethod;
	}
	/**
	 * http请求地址
	 **/
	public String getReqUrl() {
		return this.reqUrl;
	}
	/**
	 * 客户端信息
	 **/
	public String getClientInfo() {
		return this.clientInfo;
	}
	/**
	 * 归属部门编号
	 **/
	public String getDeptid() {
		return this.deptid;
	}
	/**
	 * 归属部门名称
	 **/
	public String getDeptName() {
		return this.deptName;
	}


	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}