package com.mdp.sms.client.service;

import com.mdp.core.api.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public  class SmsCodeServerRedisCacheService implements Cache<String> {

		@Autowired
		RedisTemplate redisTemplate;
	  
	String getSmsCodeCacheKey() {
		 
		return "mdp_sms_ServerSmsCode";
	}
	 

	@Override
	public void put(String key, String smsCode) {
		redisTemplate.opsForValue().set(getSmsCodeCacheKey()+":"+key, smsCode, 5, TimeUnit.MINUTES);
		
	}

	@Override
	public String get(String key) {
		 
		return (String) redisTemplate.opsForValue().get(getSmsCodeCacheKey()+":"+key);
	}

	@Override
	public void remove(String key) {
		redisTemplate.opsForValue().set(getSmsCodeCacheKey()+":"+key, null);
		
	}

	@Override
	public boolean containsKey(String key) {
		 
		return  redisTemplate.opsForValue().get(getSmsCodeCacheKey()+":"+key)==null?false:true;
	}

	@Override
	public void refresh() {
		 
		
	}

	@Override
	public boolean containsValue(String value) {
		 
		return false;
	}

	@Override
	public boolean expire(long milliseconds) {
		 
		return false;
	}
 }