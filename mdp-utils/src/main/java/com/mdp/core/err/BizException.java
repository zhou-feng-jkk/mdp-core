package com.mdp.core.err;

import com.mdp.core.entity.LangTips;
import com.mdp.core.entity.Tips;

/**
 * 统一的业务异常类<br>
 * 在进行任何业务判断时,如果需要通知客户端相关的信息,异常或者提示信息都可以通过抛出此类达到目的<br>
 * 实例如下:<br>
 * 
 * 例如：<br>
 * 
 * public Map querySysAccount(Map p){         <br>
 * 		if(null==p.get("password")){        <br>
 * 			throw new BizException("userDetailsService270","您输入的密码不正确,请重新输入,您还可以再试3次"); <br>
 * 		}else{<br>
 * 			.......<br>
 * 		}<br>
 * }<br>
 * @param tipscode 技术错误码在编码阶段由开发人员自行定义,一般由类名+代码行数组成.首字母小写,此错误码不会展现给用户
 * @param errFieldName 错误字段. 告诉客户端发生错误的具体字段信息,方便客户端进行友好提示
 * @param msg 消息 如 您输入的密码不正确,请重新输入,您还可以再试1次.	 
 * @author cyc 20150122
 * @since 1.0.0
 * 
 */
public class BizException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	Tips tips;
	
	Object[] args;
	

	/**
	 * 统一的业务异常类<br>
	 * 在进行任何业务判断时,如果需要通知客户端相关的信息,异常或者提示信息都可以通过抛出此类达到目的<br>
	 * 实例如下:<br>
	 * 
	 * 例如：<br>
	 * 
	 * public Map querySysAccount(Map p){         <br>
	 * 		if(null==p.get("password")){        <br>
	 * 			throw new BizException("您输入的密码不正确,请重新输入,您还可以再试3次"); <br>
	 * 		}else{<br>
	 * 			.......<br>
	 * 		}<br>
	 * }<br>
	 * @param msg 消息 如 您输入的密码不正确,请重新输入,您还可以再试1次.	 
	 */
	public BizException(String msg) {
		super(msg);
		tips = LangTips.errMsg(null,msg);
	}

	public BizException(Tips tips) {
		super(tips.getMsg());
		this.tips=tips;
	}
	
	/**
	 * 统一的业务异常类<br>
	 * 在进行任何业务判断时,如果需要通知客户端相关的信息,异常或者提示信息都可以通过抛出此类达到目的<br>
	 * 实例如下:<br>
	 * 
	 * 例如：<br>
	 * 
	 * public Map querySysAccount(Map p){         <br>
	 * 		if(null==p.get("password")){        <br>
	 * 			throw new BizException("userDetailsService270","您输入的密码不正确,请重新输入,您还可以再试3次"); <br>
	 * 		}else{<br>
	 * 			.......<br>
	 * 		}<br>
	 * }<br>
	 * @param tipscode 技术错误码在编码阶段由开发人员自行定义,一般由类名+代码行数组成.首字母小写,此错误码不会展现给用户
	 * @param msg 消息 如 您输入的密码不正确,请重新输入,您还可以再试1次.	 
	 */
	public BizException(String tipscode,String msg) {
		super(msg);
		tips = LangTips.errMsg(tipscode,msg);
	}
	/**
	 * 统一的业务异常类<br>
	 * 在进行任何业务判断时,如果需要通知客户端相关的信息,异常或者提示信息都可以通过抛出此类达到目的<br>
	 * 实例如下:<br>
	 * 
	 * 例如：<br>
	 * 
	 * public Map querySysAccount(Map p){         <br>
	 * 		if(null==p.get("password")){        <br>
	 * 			throw new BizException("userDetailsService270","您输入的密码不正确,请重新输入,您还可以再试3次"); <br>
	 * 		}else{<br>
	 * 			.......<br>
	 * 		}<br>
	 * }<br>
	 * @param tipscode 技术错误码在编码阶段由开发人员自行定义,一般由类名+代码行数组成.首字母小写,此错误码不会展现给用户
	 * @param errFieldName 错误字段. 告诉客户端发生错误的具体字段信息,方便客户端进行友好提示
	 * @param msg 消息 如 您输入的密码不正确,请重新输入,您还可以再试1次.	 
	 */
	public BizException(String tipscode,String errFieldName,String msg) {
		super(msg);
		tips = LangTips.errMsg(tipscode,msg).put("errFieldName",errFieldName);
	}
	public Tips getTips() {
		return tips;
	}
	public void setTips(Tips tips) {
		this.tips = tips;
	}

	public Object[] getArgs() {
		return args;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}

	@Override
	public String toString() {
		
		return tips.toString();
	}
}
