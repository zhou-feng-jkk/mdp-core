package com.mdp.core.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/***
 * 序列号格式化工具，如给定 {time}{date:yyyyMMddHHmmssS}{rand:2}  结果为：14768373247162016101908352471601<br>
 * {time}                  					--> 1476837324716<br>
 * {date:yyyyMMddHHmmssS}  					--> 20161019083524716<br>
 * {date62:yyyyMMddHHmmssS}                 --> aldkfdjfkgjla 将日期按62进制表示{DATE62:yyyyMMddHHmmssSS} <br>
 * {rand:2}               					--> 01<br>
 * {time}{date:yyyyMMddHHmmssS}{rand:2}		--> 14768373247162016101908352471601<br>
 * 
 **/
public class SequenceFormat {
	/**{time}**/
	private static final String TIME = "time";

	/**{time}**/
	private static final String TIMESTAMP = "timestamp";

	/**{DATE:yyyyMMddHHmmssSS}***/
	private static final String DATE = "date";
	/**将日期按62进制表示{DATE62:yyyyMMddHHmmssSS}***/
	private static final String DATE62 = "date62";
	/**随机数字{rand:位数}**/
	private static final String RAND = "rand";
	/**随机数字及字母{rands:位数}**/
	private static final String RANDS = "rands";
	
	private static Date currentDate = null;
	
	public static String parse ( String input ) {
		
		Pattern pattern = Pattern.compile( "\\{([^\\}]+)\\}", Pattern.CASE_INSENSITIVE  );
		Matcher matcher = pattern.matcher(input);
		
		SequenceFormat.currentDate = new Date();
		
		StringBuffer sb = new StringBuffer();
		
		while ( matcher.find() ) {
			
			matcher.appendReplacement(sb, SequenceFormat.getString( matcher.group( 1 ) ) );
			
		}
		
		matcher.appendTail(sb);
		
		return sb.toString().toUpperCase();
	}
	
	/**
	 * 格式化路径, 把windows路径替换成标准路径
	 * @param input 待格式化的路径
	 * @return 格式化后的路径
	 */
	public static String format ( String input ) {
		
		return input.replace( "\\", "/" );
		
	}

	public static String parse ( String input, String filename ) {
	
		Pattern pattern = Pattern.compile( "\\{([^\\}]+)\\}", Pattern.CASE_INSENSITIVE  );
		Matcher matcher = pattern.matcher(input);
		String matchStr = null;
		
		SequenceFormat.currentDate = new Date();
		
		StringBuffer sb = new StringBuffer();
		
		while ( matcher.find() ) {
			
			matchStr = matcher.group( 1 );
			if ( matchStr.indexOf( "filename" ) != -1 ) {
				filename = filename.replace( "$", "\\$" ).replaceAll( "[\\/:*?\"<>|]", "" );
				matcher.appendReplacement(sb, filename );
			} else {
				matcher.appendReplacement(sb, SequenceFormat.getString( matchStr ) );
			}
			
		}
		
		matcher.appendTail(sb);
		
		return sb.toString();
	}
		
	private static String getString ( String pattern ) {
		String patternStr=pattern.toLowerCase();
		// time 处理
		if ( patternStr.indexOf( SequenceFormat.TIMESTAMP ) != -1 ) {
			return SequenceFormat.getTimestamp();
		}else if ( patternStr.indexOf( SequenceFormat.TIME ) != -1 ) {
			return SequenceFormat.getTime();
		}else if ( patternStr.indexOf( SequenceFormat.DATE62 ) != -1 ) {
			return SequenceFormat.getDate62(pattern);
		} else if ( patternStr.indexOf( SequenceFormat.DATE ) != -1 ) {
			return SequenceFormat.getDate(pattern);
		} else if ( patternStr.indexOf( SequenceFormat.RANDS ) != -1 ) {
			return SequenceFormat.getStringRandom( pattern );
		} else if ( patternStr.indexOf( SequenceFormat.RAND ) != -1 ) {
			return SequenceFormat.getRandom( pattern );
		}
		
		return pattern;
		
	}

	private static String getTimestamp () {
		return System.currentTimeMillis() + "";
	}
	private static String getTime () {
		return (System.currentTimeMillis()/1000)+"";
	}
	private static String getFullYear () {
		return new SimpleDateFormat( "yyyy" ).format( SequenceFormat.currentDate );
	}
	
	private static String getYear () {
		return new SimpleDateFormat( "yy" ).format( SequenceFormat.currentDate );
	}
	
	private static String getMonth () {
		return new SimpleDateFormat( "MM" ).format( SequenceFormat.currentDate );
	}
	
	private static String getDay () {
		return new SimpleDateFormat( "dd" ).format( SequenceFormat.currentDate );
	}
	
	private static String getHour () {
		return new SimpleDateFormat( "HH" ).format( SequenceFormat.currentDate );
	}
	
	private static String getMinute () {
		return new SimpleDateFormat( "mm" ).format( SequenceFormat.currentDate );
	}
	
	private static String getSecond () {
		return new SimpleDateFormat( "ss" ).format( SequenceFormat.currentDate );
	}
	private static String getDate (String pattern) {
		pattern = pattern.split( ":" )[ 1 ].trim();
		return new SimpleDateFormat(pattern).format( SequenceFormat.currentDate );
	}
	private static String getDate62(String pattern) {
		//pattern = pattern.split( ":" )[ 1 ].trim();
		return TentoN.tentoN(((System.currentTimeMillis()-1515867606995L)), 62);
	}
	
	
	private static String getRandom ( String pattern ) {
		
		int length = 0;
		pattern = pattern.split( ":" )[ 1 ].trim();
		
		length = Integer.parseInt( pattern );
		
		return ( Math.random() + "" ).substring( 2, length+2 ).replaceAll("0", "8");
		
	}
	private static String getStringRandom ( String pattern ) {
		
		int length = 0;
		pattern = pattern.split( ":" )[ 1 ].trim();
		
		length = Integer.parseInt( pattern );
		
		return getStringRandom(length );
		
	}
	
	public static void main(String[] args) {
		
		String pattern ="{time}";
		String pattern2 ="{date:yyyyMMddHHmmss}";
		String pattern3 ="{date:yyyyMMddHHmmssS}";
		String pattern4 ="{date62:yyMMddHH}{rands:2}{rand:2}";
		String pattern5 ="{timestamp}";
		String result=SequenceFormat.parse(pattern);
		String result2=SequenceFormat.parse(pattern2);
		String result3=SequenceFormat.parse(pattern3);
		String result4=SequenceFormat.parse(pattern4);
		String result5=SequenceFormat.parse(pattern5);
		//System.out.println(result);
		//System.out.println(result2);
		//System.out.println(result3);
		//System.out.println(result4);
		
		long a = System.currentTimeMillis();  
		System.out.println(a);
		for(int i = 0;i<1000;i++){
			result4=SequenceFormat.parse(pattern4);
			System.out.println(result4);
		}
		long b = System.currentTimeMillis();  
        System.out.println("毫秒："+(b-a));  
        System.out.println(b);
        System.out.println(Math.random());

	}
	
    //生成随机数字和字母,  
    public static String getStringRandom(int length) {  
          
        String val = "";  
        Random random = new Random();  
          
        //参数length，表示生成几位随机数  
        for(int i = 0; i < length; i++) {  
              
            String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";  
            //输出字母还是数字  
            if( "char".equalsIgnoreCase(charOrNum) ) {  
                //输出是大写字母还是小写字母  
                int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;  
                char c= (char)(random.nextInt(26) + temp);  
                if(c=='o'||c=='O'){
                	c='a';
                }else if(c=='l'||c=='L'){
                	c='b';
                }
                val+=c;
            } else if( "num".equalsIgnoreCase(charOrNum) ) {  
            	int no=random.nextInt(10);
            	if(no==0){
            		no=8;
            	}
                val += String.valueOf(no);  
            }  
        }  
        return val;  
    }  
    
    
    
}
